package main

import (
	"net"
	"strings"
	"time"

	dhcp "github.com/krolaw/dhcp4"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/nex/pkg"
	"gitlab.com/mergetb/tech/rtnl"
)

//Handler handler struct for dhcp server
type Handler struct{}

func main() {

	log.SetLevel(log.InfoLevel)
	log.Infof("nex-dhcpd: %s", nex.Version)

	nex.Init()

	err := nex.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	fields := log.Fields{
		"dhcp settings": nex.Current.Dhcpd,
		"etcd settings": nex.Current.Etcd,
		"nexd settings": nex.Current.Nexd,
		"debug":         nex.Current.Debug,
		"trace":         nex.Current.Trace,
		"max message":   nex.MaxMessageSize,
	}
	log.WithFields(fields).Infof("Starting nex-dhcp")

	if nex.Current.Debug {
		log.SetLevel(log.DebugLevel)
	}
	if nex.Current.Trace {
		log.SetLevel(log.TraceLevel)
	}

	handler := &Handler{}

	ifx := determineInterface()

	log.Infof("listening on %s", ifx)
	cnx, err := NewRawListener(ifx)
	if err != nil {
		log.Fatal(err)
	}
	log.Fatal(dhcp.Serve(cnx, handler))
}

func determineInterface() string {

	// If an interface name has been supplied, use that
	if nex.Current.Dhcpd.Interface != "" {

		return nex.Current.Dhcpd.Interface

	}

	// If an interface index has been supplied, use that
	if nex.Current.Dhcpd.InterfaceIndex != 0 {

		ctx, err := rtnl.OpenDefaultContext()
		if err != nil {
			log.Fatal(err)
		}

		links, err := rtnl.ReadLinks(ctx, nil)
		if err != nil {
			log.Fatal(err)
		}

		if nex.Current.Dhcpd.InterfaceIndex >= len(links) {
			log.Fatal("provided interface index is beyond number of interfaces")
		}

		return links[0].Info.Name

	}

	// Otherwise listen on loopback interface
	log.Warn("no interface provided, listening on loopback")
	return "lo"

}

//ServeDHCP dhcp server
func (h *Handler) ServeDHCP(
	pkt dhcp.Packet,
	msgType dhcp.MessageType,
	options dhcp.Options,
) dhcp.Packet {

	fields := log.Fields{}

	switch msgType {

	case dhcp.Discover:
		fields["mac"] = pkt.CHAddr()
		log.WithFields(fields).Debug("discover: start")

		// Collect network information
		network, err := nex.FindMacNetwork(pkt.CHAddr())
		if err != nil {
			log.WithError(err).Error("discover: error")
			return nil
		}
		if network == nil {
			log.WithFields(fields).Warn("discover: has no net")
			return nil
		}
		fields["network"] = network.Name
		log.WithFields(fields).Debug("found network")

		response := func(server, addr net.IP, options []dhcp.Option) dhcp.Packet {

			fields["addr"] = addr
			log.WithFields(fields).Debug("discover: OK")

			opts := ToOpt(network.Options)

			_, subnetCIDR, err := net.ParseCIDR(network.Subnet4)
			if err != nil {
				log.Errorf("bad subnet: %v", err)
				return nil
			}

			opts = append(opts, dhcp.Option{
				Code:  dhcp.OptionSubnetMask,
				Value: subnetCIDR.Mask,
			})

			for _, x := range network.Gateways {
				ip := net.ParseIP(x)
				opts = append(opts, dhcp.Option{
					Code:  dhcp.OptionRouter,
					Value: ip.To4(),
				})
			}

			for _, x := range network.Nameservers {
				ip := net.ParseIP(x)
				opts = append(opts, dhcp.Option{
					Code:  dhcp.OptionDomainNameServer,
					Value: ip.To4(),
				})
			}

			if network.Domain != "" {
				cn := compressedDNSName(network.Domain)
				opts = append(opts, dhcp.Option{
					Code:  dhcp.OptionDomainSearch,
					Value: cn,
				})
			}

			return dhcp.ReplyPacket(
				pkt, dhcp.Offer, server, addr,
				time.Duration(network.LeaseDuration)*time.Second,
				opts)
		}

		// If there is already an address use that
		addr, err := nex.FindMacIpv4(pkt.CHAddr())
		if err != nil && !nex.IsNotFound(err) {
			log.WithError(err).Errorf("discover: mac error")
			return nil
		}
		if addr != nil {
			fields["found"] = addr.String()

			return response(
				net.ParseIP(network.Dhcp4Server).To4(),
				addr,
				ToOpt(network.Options),
			)
		}

		// If no address was found allocate a new one
		addr, err = nex.NewLease4(pkt.CHAddr(), network.Name, pkt)
		if err != nil {
			log.WithError(err).Errorf("discover: lease error")
			return nil
		}
		if addr != nil {
			return response(
				net.ParseIP(network.Dhcp4Server).To4(),
				addr,
				ToOpt(network.Options),
			)
		}

		log.WithFields(fields).Error("address pool depleted")
		//Address is nil, so no discover response will go out

	case dhcp.Request:

		fields["mac"] = pkt.CHAddr()
		log.WithFields(fields).Debug("request: start")

		rqAddr := net.IP(pkt.CIAddr())

		network, err := nex.FindMacNetwork(pkt.CHAddr())
		if err != nil {
			log.WithError(err).Error("request: find mac net error")
			return nil
		}
		if network == nil {
			log.WithFields(fields).Warn("request: has no net")
			return nil
		}
		server := net.ParseIP(network.Dhcp4Server).To4()

		addr, err := nex.FindMacIpv4(pkt.CHAddr())
		if err != nil {
			log.WithError(err).Error("request: find mac ipv4 error")
			return nil
		}
		if addr == nil {

			log.WithFields(fields).Warn("request: no address found")
			return dhcp.ReplyPacket(pkt, dhcp.NAK, server, nil, 0, nil)

		}

		_, subnetCIDR, err := net.ParseCIDR(network.Subnet4)
		if err != nil {
			log.Errorf("bad subnet: %v", err)
			return nil
		}

		if addr.Equal(rqAddr) || rqAddr.Equal(net.IPv4(0, 0, 0, 0)) {
			log.WithFields(fields).Debug("request: OK")

			nex.RenewLease(pkt.CHAddr(), network.Name)

			opts := ToOpt(network.Options)

			opts = append(opts, dhcp.Option{
				Code:  dhcp.OptionSubnetMask,
				Value: subnetCIDR.Mask,
			})

			for _, x := range network.Gateways {
				ip := net.ParseIP(x)
				opts = append(opts, dhcp.Option{
					Code:  dhcp.OptionRouter,
					Value: ip.To4(),
				})
			}

			for _, x := range network.Nameservers {
				ip := net.ParseIP(x)
				opts = append(opts, dhcp.Option{
					Code:  dhcp.OptionDomainNameServer,
					Value: ip.To4(),
				})
			}

			if network.Domain != "" {
				cn := compressedDNSName(network.Domain)
				opts = append(opts, dhcp.Option{
					Code:  dhcp.OptionDomainSearch,
					Value: cn,
				})
			}

			pkt := dhcp.ReplyPacket(
				pkt, dhcp.ACK, server, addr,
				time.Duration(network.LeaseDuration)*time.Second,
				opts)
			// add next-server option to packet
			if net.ParseIP(network.Siaddr) != nil {
				// does not support ipv6
				pkt.SetSIAddr(net.ParseIP(network.Siaddr))
			}
			return pkt

		}

		// Received dhcp request for unknown address
		fields["rqAddr"] = rqAddr
		fields["addr"] = addr
		log.WithFields(fields).Warn("request: unsolicited IP")
		return dhcp.ReplyPacket(pkt, dhcp.NAK, server, nil, 0, nil)

	case dhcp.Release:
		log.Debugf("release: %s", pkt.CHAddr())

	case dhcp.Decline:
		log.Debugf("decline: %s", pkt.CHAddr())

	}

	return nil

}

func compressedDNSName(name string) []byte {
	parts := strings.Split(name, ".")

	var payload []byte

	for _, p := range parts {
		payload = append(payload, byte(len(p)))
		payload = append(payload, []byte(p)...)
	}

	payload = append(payload, byte(0))

	return payload
}

//ToOpt converts nex options into dhcp options
func ToOpt(opts []*nex.Option) []dhcp.Option {
	var result []dhcp.Option
	for _, x := range opts {
		result = append(result, dhcp.Option{
			Code:  dhcp.OptionCode(x.Number),
			Value: []byte(x.Value),
		})
	}
	return result
}
